# Используем базовый образ Python
FROM python:3.11.2

# Устанавливаем переменную среды, чтобы не выводить лишние предупреждения
ENV PYTHONUNBUFFERED 1

# Устанавливаем рабочую директорию внутри контейнера
WORKDIR /app

# Копируем файлы сервера в контейнер
COPY server.py /app/server.py
COPY requirements.txt /app/requirements.txt

# Устанавливаем зависимости Flask из requirements.txt
RUN pip install -r requirements.txt

# Определяем порт, на котором будет работать сервер
EXPOSE 5000

# Запускаем сервер Flask
CMD ["python", "server.py"]
